from django.db import models

from course.models import Episode
from student_dashboard.models import Student
from teacher_dashboard.models import Teacher


class Questions(models.Model):
    sender = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="creator", default=1)
    related_episode = models.ForeignKey(Episode, related_name='episode_question', on_delete=models.CASCADE)
    question = models.TextField(null=False)
    creation_time = models.BigIntegerField(null=False, blank=False, default=0)
    is_answered = models.BooleanField(null=False, default=False)

    def __str__(self):
        return "{}: {}".format(self.sender.pk, self.question)


class Answer(models.Model):
    sender = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="creator", default=1)
    answer = models.TextField(null=False)
    creation_time = models.BigIntegerField(null=False, blank=False, default=0)
    question = models.ForeignKey(Questions, related_name='question_answer', on_delete=models.CASCADE)

    def __str__(self):
        return "{}: {}".format(self.sender.pk, self.answer)
