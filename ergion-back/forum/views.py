import time

from rest_framework import generics, status
from rest_framework.response import Response

from student_dashboard.models import Student
from student_dashboard.models import Student,Notifications
from teacher_dashboard.models import Teacher
from .models import Questions, Answer
from .serializers import EpisodeQuestionsSerializers, EpisodeAnswerSerializers


class EpisodeAnswerView(generics.ListCreateAPIView, generics.DestroyAPIView, generics.UpdateAPIView):
    serializer_class = EpisodeAnswerSerializers

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        tch = Teacher.objects.filter(user=request.user).first()
        course = serializer.validated_data['question'].related_episode.chapter.course
        if tch == course.instructor:
            serializer.save(sender_id=tch.id, creation_time=int(time.time() * 1000))
            question = Questions.objects.get(pk=request.data['question'])
            question.is_answered = True
            question.save()
            headers = self.get_success_headers(serializer.data)
            Notifications.objects.create(student=question.sender, user=request.user, created_at=int(time.time() * 1000),
                                         type=2, question=serializer.validated_data['question'])
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            headers = self.get_success_headers(serializer.data)
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE, headers=headers)

    def delete(self, request, *args, **kwargs):
        instance = Answer.objects.get(pk=request.query_params['answer_id'])
        question = Questions.objects.get(id=instance.question_id)
        if instance.sender_id == Teacher.objects.filter(user=self.request.user).first().id:
            instance.delete()
            if question.question_answer.count() == 0:
                question.is_answered = False
                question.save()

            return Response({'Message': 'this answer deleted successfully'}, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def update(self, request, *args, **kwargs):
        instance = Answer.objects.get(pk=request.query_params['answer_id'])
        if instance.sender_id == Teacher.objects.filter(user=self.request.user).first().id:
            serializer = self.get_serializer(instance, data=request.data)
            serializer.initial_data['question'] = instance.question_id
            serializer.is_valid(raise_exception=True)
            serializer.save()
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def get(self, request, *args, **kwargs):
        queryset = []
        data = EpisodeAnswerSerializers(queryset, many=True).data
        http_host = request.META['HTTP_HOST']
        if 'course_id' in list(self.request.query_params.keys()):
            queryset = Questions.objects.filter(
                related_episode__chapter__course_id=self.request.query_params['course_id']).order_by('-creation_time')
            data = EpisodeQuestionsSerializers(queryset, many=True).data
            for i in data:
                answer = i.get('answer')
                for a in answer:
                    if a['sender_profile_picture'] is not None:
                        a['sender_profile_picture'] = ('http://{}' + a['sender_profile_picture']).format(http_host)
                    else:
                        a['sender_profile_picture'] = ''
        elif 'answer_id' in list(self.request.query_params.keys()):
            queryset = Answer.objects.filter(id=self.request.query_params['answer_id']).order_by('-creation_time')
            data = EpisodeAnswerSerializers(queryset, many=True).data
        for i in data:
            if i['sender_profile_picture'] is not None:
                i['sender_profile_picture'] = ('http://{}' + i['sender_profile_picture']).format(http_host)
            else:
                i['sender_profile_picture'] = ''
        return Response(data)


class EpisodeQuestionView(generics.ListCreateAPIView, generics.DestroyAPIView, generics.UpdateAPIView):
    queryset = Questions.objects.all().order_by('id')
    serializer_class = EpisodeQuestionsSerializers

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        stu = Student.objects.filter(user=request.user).first()
        course = serializer.validated_data['related_episode'].chapter.course
        if stu in list(course.course_students.all()):
            serializer.save(sender_id=stu.id, creation_time=int(time.time() * 1000))
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            headers = self.get_success_headers(serializer.data)
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE, headers=headers)

    def delete(self, request, *args, **kwargs):
        instance = Questions.objects.get(pk=request.query_params['question_id'])
        if instance.sender_id == Student.objects.filter(user=self.request.user).first().id:
            if instance.question_answer.count() == 0:
                instance.delete()
            else:
                return Response({'Message': 'this question has an answer'})
            return Response({'Message': 'this question deleted successfully'}, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def update(self, request, *args, **kwargs):
        instance = Questions.objects.get(pk=request.query_params['question_id'])
        if instance.sender_id == Student.objects.filter(user=self.request.user).first().id:

            if instance.question_answer.count() == 0:
                serializer = self.get_serializer(instance, data=request.data)
                serializer.initial_data['related_episode'] = instance.related_episode_id
                serializer.is_valid(raise_exception=True)
            else:
                return Response({'Message': 'this question has an answer'})
            serializer.save()
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            return Response({'Message': 'you have not access'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def get_queryset(self):
        queryset = []
        if 'course_id' in list(self.request.query_params.keys()):
            queryset = Questions.objects.filter(
                related_episode__chapter__course_id=self.request.query_params['course_id']).exclude(
                question_answer__isnull=True).order_by('-creation_time')
        return queryset
