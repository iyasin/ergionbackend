from rest_framework import permissions

from teacher_dashboard.models import Teacher
from course.models import Course
from .models import Questions
from users.models import CustomUser


class IsCourseTeacher(permissions.BasePermission):

    def has_permission(self, request, view):
        question = Questions.objects.get(pk=request.data['question'])
        course = question.related_episode.chapter.course
        return course.instructor_id == Teacher.objects.filter(user_id=request.user.id).first()



