# Generated by Django 3.1.2 on 2020-12-16 16:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('forum', '0001_initial'),
        ('student_dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='sender',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='creator', to='student_dashboard.student'),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question_answer', to='forum.questions'),
        ),
    ]
