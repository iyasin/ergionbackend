from django.urls import path

from forum import views

urlpatterns = [
    path('episode-question/', views.EpisodeQuestionView.as_view()),
    path('episode-answer/', views.EpisodeAnswerView.as_view()),
]
