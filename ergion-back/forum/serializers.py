from rest_framework import serializers
from users.models import CustomUser
from course.models import Chapter, Episode
from course.models import Course, News, Comment, File, Post
from .models import Questions, Answer


class EpisodeAnswerSerializers(serializers.ModelSerializer):
    sender_firstname = serializers.CharField(source='sender.user.firstname', required=False)
    sender_lastname = serializers.CharField(source='sender.user.lastname', required=False)
    sender_profile_picture = serializers.ImageField(source='sender.profile_picture', required=False)
    related_episode_name = serializers.CharField(source='question.related_episode.post.name', required=False)
    related_episode = serializers.IntegerField(source='question.related_episode.id', required=False)

    class Meta:
        model = Answer
        fields = (
            'id', 'sender', 'sender_firstname', 'sender_lastname', 'sender_profile_picture', 'question',
            'creation_time', 'answer', 'related_episode', 'related_episode_name')
        read_only_fields = ('id', 'sender_firstname', 'sender_lastname', 'related_episode_name', 'related_episode', 'sender_profile_picture')
        required_fields = ('answer', 'question')


class EpisodeQuestionsSerializers(serializers.ModelSerializer):
    sender_firstname = serializers.CharField(source='sender.user.firstname', required=False)
    sender_lastname = serializers.CharField(source='sender.user.lastname', required=False)
    sender_profile_picture = serializers.ImageField(source='sender.profile_picture', required=False)
    related_episode_name = serializers.CharField(source='related_episode.post.name', required=False)
    answer = EpisodeAnswerSerializers(many=True, required=False, read_only=True, source='question_answer')
    related_chapter = serializers.IntegerField(required=False, source='related_episode.chapter.pk')

    class Meta:
        model = Questions
        fields = (
            'id', 'sender', 'sender_firstname', 'sender_lastname', 'sender_profile_picture', 'question',
            'creation_time', 'related_episode', 'related_episode_name', 'is_answered', 'answer','related_chapter')
        read_only_fields = ('id', 'sender_firstname', 'sender_lastname', 'related_episode_name', 'sender_profile_picture','related_chapter')
        required_fields = ('question', 'related_episode')
