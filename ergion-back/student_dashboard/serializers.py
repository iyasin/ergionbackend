from rest_framework import serializers

from course.models import Course, Post, JoinRequest
from course.serializers import CommentSerializer, FileSerializer
from users.models import CustomUser
from .models import Student
from .models import Student , Notifications


class CoursesSerializer(serializers.HyperlinkedModelSerializer):
    instructor_id = serializers.IntegerField(source='instructor.pk', required=False)
    instructor_firstname = serializers.CharField(source='instructor.user.firstname', required=False)
    instructor_lastname = serializers.CharField(source='instructor.user.lastname', required=False)
    instructor_profile_picture = serializers.ImageField(source='instructor.profile_picture', required=False)

    class Meta:
        model = Course
        fields = (
            'id', 'instructor_id', 'instructor_firstname', 'instructor_lastname', 'instructor_profile_picture', 'name',
            'subject', 'course_cover', 'capacity', 'course_url', 'about_course', 'grade', 'is_public')
        read_only_fields = (
            'instructor_id', 'instructor_firstname', 'instructor_lastname', 'instructor_profile_picture', 'name',
            'subject', 'course_cover', 'capacity', 'course_url', 'about_course', 'grade', 'is_public')

    def to_representation(self, instance):
        data = super(CoursesSerializer, self).to_representation(instance)
        course = Course.objects.all().get(pk=data['id'])
        if self.get_user_id() is not None:
            if CustomUser.objects.get(pk=self.get_user_id()).role == 'S':
                data['requested'] = JoinRequest.objects.filter(course=course, student=CustomUser.objects.get(
                    pk=self.get_user_id()).Student).first() is not None
                data['joined'] = course in CustomUser.objects.get(pk=self.get_user_id()).Student.courses.all()
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class StudentSerializer(serializers.HyperlinkedModelSerializer):
    firstname = serializers.CharField(source='user.firstname', required=False)
    lastname = serializers.CharField(source='user.lastname', required=False)
    email = serializers.CharField(source='user.email', required=False)
    profile_picture = serializers.ImageField(allow_null=True, required=False)

    class Meta:
        model = Student
        fields = ('firstname', 'lastname', 'id', 'grade', 'email', 'profile_picture')
        read_only_fields = ('id',)

    def to_representation(self, data):
        data = super(StudentSerializer, self).to_representation(data)
        request = self.context.get('request', None)
        if request.method != 'GET':
            if self.initial_data is not None:
                if 'email' in list(self.initial_data.keys()):
                    data['email'] = self.initial_data['email']
                if 'firstname' in list(self.initial_data.keys()):
                    data['firstname'] = self.initial_data['firstname']
                if 'lastname' in list(self.initial_data.keys()):
                    data['lastname'] = self.initial_data['lastname']
                if 'grade' in list(self.initial_data.keys()):
                    data['grade'] = int(self.initial_data['grade'])
                if CustomUser.objects.filter(email=data['email']).first() is not None:
                    custom_user = CustomUser.objects.get(pk=self.get_user_id())
                    data['email'] = custom_user.email
                custom_user = CustomUser.objects.get(pk=self.get_user_id())
                if 'profile_picture' in list(self.initial_data.keys()):
                    student = custom_user.Student
                    student.profile_picture = self.initial_data['profile_picture']
                    student.save()
                    http_host = request.META['HTTP_HOST']
                    if student.profile_picture.name is not None:
                        data['profile_picture'] = (
                                'http://{}/media/' + student.profile_picture.name).format(
                            http_host)
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id

    def update(self, instance, validated_data):
        custom_user = CustomUser.objects.get(pk=self.get_user_id())
        student_object = custom_user.Student
        student_object.grade = validated_data.get('grade', student_object.grade)
        custom_user.firstname = validated_data.get('firstname', custom_user.firstname)
        custom_user.lastname = validated_data.get('lastname', custom_user.lastname)
        if CustomUser.objects.filter(
                email=validated_data.get('email', custom_user.email)).first() is None:
            custom_user.email = validated_data.get('email', custom_user.email)
        custom_user.save()
        student_object.save()
        return student_object


class AllCoursesSerializer(serializers.HyperlinkedModelSerializer):
    instructor_firstname = serializers.CharField(source='instructor.user.firstname', required=False)
    instructor_lastname = serializers.CharField(source='instructor.user.lastname', required=False)
    instructor_id = serializers.IntegerField(source='instructor.pk', required=False)
    instructor_profile_picture = serializers.ImageField(source='instructor.profile_picture', required=False)

    class Meta:
        model = Course
        fields = ('id', 'name', 'course_cover', 'course_url', 'capacity', 'instructor_firstname', 'instructor_lastname',
                  'instructor_id', 'instructor_profile_picture', 'is_public')
        read_only_fields = fields


class PostSerializer(serializers.HyperlinkedModelSerializer):
    instructor_id = serializers.IntegerField(source='instructor.pk', required=False)
    instructor_firstname = serializers.CharField(source='instructor.user.firstname', required=False)
    instructor_lastname = serializers.CharField(source='instructor.user.lastname', required=False)
    instructor_profile_picture = serializers.ImageField(source='instructor.profile_picture', required=False)
    likes_count = serializers.IntegerField(required=False, default=0)
    liked = serializers.BooleanField(default=False, required=False)
    course_id = serializers.IntegerField(source='course.pk', required=False)
    course_name = serializers.CharField(source='course.name', required=False)
    post_url = serializers.CharField(required=False, default='')
    course_url = serializers.CharField(source='course.course_url')
    comments_count = serializers.IntegerField(default=0, required=False)

    class Meta:
        model = Post
        fields = (
            'id', 'post_type', 'name', 'description', 'instructor_id', 'instructor_firstname', 'instructor_lastname',
            'instructor_profile_picture', 'creation_time', 'likes_count', 'liked', 'course_id', 'course_name',
            'post_url', 'course_url', 'comments_count')

    def to_representation(self, instance):
        data = super(PostSerializer, self).to_representation(instance)
        student = CustomUser.objects.get(pk=self.get_user_id()).Student
        post = Post.objects.get(pk=data['id'])
        data['likes_count'] = post.likes.count()
        data['liked'] = student in post.likes.all()
        if post.post_type == 0:
            data['episode_id'] = post.post_episode.all()[0].pk
            data['post_url'] = post.post_episode.all()[0].episode_url
        elif post.post_type == 1:
            data['news_id'] = post.post_news.all()[0].pk
            data['post_url'] = post.post_news.all()[0].news_url
        data['comments_count'] = post.comments.count()
        comments_data = list(CommentSerializer(post.comments.all(), many=True, context=self.context).data)
        sorted_comments = sorted(comments_data, key=lambda k: k['likes_count'], reverse=True)
        if len(sorted_comments) > 3:
            sorted_comments = sorted_comments[:3]
        data['comments'] = sorted_comments
        files_data = []
        for file in post.files.all():
            files_data.append(FileSerializer(file, context=self.context).data)
        data['files'] = files_data
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class StudentNotificationsSerializers(serializers.ModelSerializer):
    user_firstname = serializers.CharField(source='user.firstname', required=False)
    user_lastname = serializers.CharField(source='user.lastname', required=False)
    user_role = serializers.CharField(source='user.role', required=False)
    user_profile_picture = serializers.CharField(required=False)

    class Meta:
        model = Notifications
        fields = (
            'id', 'student', 'user', 'user_firstname', 'user_lastname', 'user_role', 'user_profile_picture', 'type',
            'created_at', 'has_seen')

    def to_representation(self, instance):
        data = super(StudentNotificationsSerializers, self).to_representation(instance)
        request = self.context.get('request', None)
        role = data['user_role']
        user = CustomUser.objects.get(id=data['user'])
        http_host = request.META['HTTP_HOST']
        if user.role == 'S':
            if str(user.Student.profile_picture.name) != '':
                data['user_profile_picture'] = ('http://{}/media/' + user.Student.profile_picture.name).format(http_host)
            else:
                data['user_profile_picture'] = ''
        elif user.role == 'T':
            if str(user.Teacher.profile_picture.name) != '':
                data['user_profile_picture'] = ('http://{}/media/' + user.Teacher.profile_picture.name).format(http_host)
            else:
                data['user_profile_picture'] = ''
        if int(instance.type) == 0 or int(instance.type) == 1:
            data['comments'] = instance.comment.comment_text
        elif int(instance.type) == 2:
            data['question'] = instance.question.question
        elif int(instance.type) == 3 or int(instance.type) == 4:
            data['course'] = instance.course.name
        # data['course'] ="{}  ,{}".format(instance.course.name,instance.type)
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id
