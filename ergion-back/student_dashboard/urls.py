from django.urls import path

from student_dashboard import views

urlpatterns = [
    path('courses/', views.StudentCoursesViewSet.as_view()),
    path('all-courses/', views.AllCoursesViewSet.as_view()),
    path('profile/', views.StudentDetailsViewSet.as_view()),
    path('suggested-courses/', views.SuggestedCoursesViewSet.as_view()),
    path('suggested-instructors/', views.SuggestedInstructorsViewSet.as_view()),
    path('news/', views.StudentNewsListViewSet.as_view()),
    path('posts/', views.PostsViewSet.as_view()),
    path('notifications/', views.StudentNotificationsView.as_view())
]
