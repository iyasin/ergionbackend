from django.contrib import admin
from .models import Student, Notifications

admin.site.register(Student)
admin.site.register(Notifications)
