from django.db import models
from users.models import CustomUser


class Student(models.Model):
    grade = models.IntegerField(null=True, blank=True)
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='Student')
    courses = models.ManyToManyField('course.Course', related_name="course_students")
    profile_picture = models.ImageField(upload_to='Uploaded/StudentProfilePictures', blank=True)

    def __str__(self):
        return self.user.email


class Participants(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='Student')
    course = models.ForeignKey('course.Course', on_delete=models.CASCADE, related_name="Course")
    creation_time = models.BigIntegerField(null=False, blank=False, default=0)


class Notifications(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='receiver')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='sender')
    CHOICES = (
        (0, 'Like'),
        (1, 'Comment'),
        (2, 'Answer'),
        (3, 'Accept'),
        (4, 'denied'),
    )
    type = models.CharField(max_length=1, choices=CHOICES, default=0)
    created_at = models.BigIntegerField(null=False, blank=False, default=0)
    has_seen = models.BooleanField(null=False, blank=False, default=False)
    comment = models.ForeignKey("course.Comment", blank=True, null=True, on_delete=models.CASCADE, related_name='related_comment')
    course = models.ForeignKey("course.Course", blank=True,null=True, on_delete=models.CASCADE, related_name='related_course')
    question = models.ForeignKey("forum.Questions", blank=True, null=True, on_delete=models.CASCADE, related_name='related_question')
