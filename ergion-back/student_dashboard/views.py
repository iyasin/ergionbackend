import time

from django.core.paginator import Paginator
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response

from course.models import Course, News, JoinRequest
from course.serializers import NewsSerializer
from teacher_dashboard.models import Teacher
from teacher_dashboard.serializers import TeacherSerializers
from users.models import CustomUser
from .models import Student, Participants
from .serializers import StudentSerializer, AllCoursesSerializer, CoursesSerializer, PostSerializer


from .models import Student, Participants, Notifications
from .serializers import StudentSerializer, AllCoursesSerializer, CoursesSerializer, PostSerializer,\
    StudentNotificationsSerializers


class StudentNotificationsView(generics.ListAPIView, generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = StudentNotificationsSerializers

    def get(self, request, *args, **kwargs):
        queryset = Notifications.objects.filter(student_id=request.user.Student.id).order_by('-created_at')
        count_unseen=Notifications.objects.filter(student_id=request.user.Student.id, has_seen=False).count()
        if 'page' in list(self.request.query_params):
            paginator = Paginator(queryset, 6)
            if int(self.request.query_params['page']) <= paginator.num_pages:
                data = paginator.page(self.request.query_params['page']).object_list
                return Response(data=
                  {
                    'has_next': int(self.request.query_params['page']) < paginator.num_pages,
                    'count_of_unseen':count_unseen,
                    'data': self.get_serializer(data, many=True).data
                  }
                )

        serializer = self.get_serializer(queryset, many=True)
        return Response(data={
            'count_of_unseen': count_unseen,
            'data': serializer.data})
    
    def put(self, request, *args, **kwargs):
        queryset = Notifications.objects.filter(student_id=request.user.Student.id).order_by('-created_at')
        for instance in queryset:
            if instance.has_seen is False:
                instance.has_seen = True
                instance.save()

        count_unseen = Notifications.objects.filter(student_id=request.user.Student.id, has_seen=False).count()
        return Response(data={
            'count_of_unseen': count_unseen}
        )

class SuggestedCoursesViewSet(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = AllCoursesSerializer

    def get_queryset(self):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student = custom_user.Student
        user_grade = student.grade
        queryset = [course for course in Course.objects.all() if
                    (course.grade == user_grade and course not in student.courses.all())]
        if len(queryset) > 4:
            queryset = queryset[:4]
        return queryset


class AllCoursesViewSet(generics.ListAPIView):
    serializer_class = AllCoursesSerializer

    def get_queryset(self):
        if 'substring' in list(self.request.query_params.keys()):
            search_param = self.request.query_params['substring']
            queryset = [course for course in Course.objects.all() if (
                    (search_param.lower() in course.name.lower()) or (
                    search_param.lower() in course.instructor.user.firstname.lower() + " " + course.instructor.user.lastname.lower()) or (
                            search_param.lower() in course.subject.lower()))]
            return queryset
        return Course.objects.all()


class StudentCoursesViewSet(generics.ListAPIView, generics.UpdateAPIView, generics.DestroyAPIView):
    serializer_class = CoursesSerializer

    def get_queryset(self):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student_object = custom_user.Student
        queryset = student_object.courses
        return queryset

    def put(self, request, *args, **kwargs):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student_object = custom_user.Student
        course = Course.objects.filter(pk=int(request.data.get('course_id'))).first()
        if course is not None:
            if course.is_public:
                student_object.courses.add(course)
                Participants.objects.create(course_id=int(request.data.get('course_id')),
                                            student_id=student_object.id,
                                            creation_time=int(time.time() * 1000))
            else:
                JoinRequest.objects.create(student=self.request.user.Student, course=course,
                                           instructor=course.instructor)
                serializer = self.get_serializer(course, many=False)
                course_data = serializer.data
                return Response(course_data)
        serializer = self.get_serializer(student_object.courses, many=True)
        course_data = serializer.data
        return Response(course_data)

    def destroy(self, request, *args, **kwargs):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student_object = custom_user.Student
        course = Course.objects.filter(pk=int(self.request.query_params['course_id'])).first()
        if course is not None:
            if course in student_object.courses.all():
                student_object.courses.remove(course)
                return Response("Left course.")
            else:
                join_request = JoinRequest.objects.get(student=student_object, course=course)
                join_request.delete()
                return Response("Request cancelled.")
        return Response("Course does not exist.")


class StudentDetailsViewSet(generics.ListAPIView, generics.UpdateAPIView):
    serializer_class = StudentSerializer
    queryset = []

    def get(self, request, *args, **kwargs):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student_object = custom_user.Student
        queryset = [student_object, ]
        serializer = self.get_serializer(queryset, many=True)
        user_data = serializer.data[0]
        return Response(user_data)

    def get_object(self):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        student_object = custom_user.Student
        return student_object


class StudentNewsListViewSet(generics.ListAPIView):
    serializer_class = NewsSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        student = Student.objects.get(user=self.request.user)
        queryset = [news for news in News.objects.all() if news.course in student.courses.all()]
        return queryset


class PostsViewSet(generics.ListAPIView):
    serializer_class = PostSerializer

    def get(self, request, *args, **kwargs):
        user = CustomUser.objects.get(pk=self.request.user.id)
        student = user.Student
        courses = student.courses.all()
        data = []
        for course in courses:
            for post in course.course_posts.all():
                serializer = self.get_serializer(post)
                data.append(serializer.data)
        sorted_data = sorted(data, key=lambda k: k['creation_time'])[::-1]
        if 'page' in list(self.request.query_params):
            paginator = Paginator(sorted_data, 4)
            if int(self.request.query_params['page']) <= paginator.num_pages:
                data = paginator.page(self.request.query_params['page']).object_list
                return Response(data={
                    'has_next': int(self.request.query_params['page']) < paginator.num_pages,
                    'data': data
                })
        return Response(data)


class SuggestedInstructorsViewSet(generics.ListAPIView):
    serializer_class = TeacherSerializers
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        queryset = list(Teacher.objects.all().order_by('popularity_index'))[::-1]
        if len(queryset) > 4:
            queryset = queryset[:4]
        return queryset
