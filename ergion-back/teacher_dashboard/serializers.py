from rest_framework import serializers

from course.models import Course, JoinRequest
from course.serializers import CourseSerializer
from forum.models import Questions
from forum.serializers import EpisodeQuestionsSerializers
from users.models import CustomUser
from .models import Teacher


class TeacherProfileSerializers(serializers.ModelSerializer):
    firstname = serializers.CharField(source='user.firstname', required=False, read_only=True)
    lastname = serializers.CharField(source='user.lastname', required=False, read_only=True)
    email = serializers.EmailField(source='user.email', required=False, read_only=True)

    class Meta:
        model = Teacher
        fields = ['id', 'teacher_type', 'profile_picture', 'firstname', 'lastname', 'email', 'scholar_email',
                  'personal_description', 'academy_name', 'work_experience']

    def to_representation(self, instance):
        data = super(TeacherProfileSerializers, self).to_representation(instance)
        request = self.context.get('request', None)
        course_data = CourseSerializer(instance.Creator, many=True, context=self.context).data
        http_host = request.META['HTTP_HOST']
        for crs in course_data:
            if crs['course_cover'] is not None:
                crs['course_cover'] = crs['course_cover']
            else:
                crs['course_cover'] = None
        course = Course.objects.filter(instructor_id=instance.id).exclude(course_students__isnull=True).values(
            'course_students').distinct().count()
        data['course_count'] = instance.Creator.count()
        data['courses'] = course_data
        data['student_count'] = course
        data['count_of_questions'] = Questions.objects.filter(related_episode__post__instructor_id=instance.id).exclude(
            is_answered=True).count()
        return data


class TeacherSerializers(serializers.ModelSerializer):
    firstname = serializers.CharField(source='user.firstname', required=False, read_only=True)
    lastname = serializers.CharField(source='user.lastname', required=False, read_only=True)
    email = serializers.EmailField(source='user.email', required=False, read_only=True)

    class Meta:
        model = Teacher
        fields = ['id', 'teacher_type', 'profile_picture', 'firstname', 'lastname', 'email', 'scholar_email',
                  'personal_description', 'academy_name', 'work_experience']

    def to_representation(self, data):
        data = super(TeacherSerializers, self).to_representation(data)
        request = self.context.get('request', None)
        if request.method != 'GET':
            if 'email' in list(self.initial_data.keys()):
                data['email'] = self.initial_data['email']
            if 'firstname' in list(self.initial_data.keys()):
                data['firstname'] = self.initial_data['firstname']
            if 'lastname' in list(self.initial_data.keys()):
                data['lastname'] = self.initial_data['lastname']
            if CustomUser.objects.filter(email=data['email']).first() is not None:
                custom_user = CustomUser.objects.get(pk=self.get_user_id())
                data['email'] = custom_user.email
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class TeacherTimeLineSerializers(serializers.ModelSerializer):
    questions = EpisodeQuestionsSerializers(source='posts__post_episode__episode_question', many=True, read_only=True)
    firstname = serializers.CharField(source='user.firstname', required=False, read_only=True)
    lastname = serializers.CharField(source='user.lastname', required=False, read_only=True)
    email = serializers.EmailField(source='user.email', required=False, read_only=True)

    class Meta:
        model = Teacher
        fields = ['id', 'questions', 'teacher_type', 'profile_picture', 'firstname', 'lastname', 'email',
                  'scholar_email', 'personal_description', 'academy_name', 'work_experience']


class JoinRequestsSerializer(serializers.HyperlinkedModelSerializer):
    course_id = serializers.IntegerField(source='course.pk', required=False)
    course_name = serializers.CharField(source='course.name', required=False)
    student_id = serializers.IntegerField(source='student.pk', required=False)
    student_firstname = serializers.CharField(source='student.user.firstname', required=False)
    student_lastname = serializers.CharField(source='student.user.lastname', required=False)
    student_profile_picture = serializers.ImageField(source='student.profile_picture', required=False)

    class Meta:
        model = JoinRequest
        fields = ('id', 'course_id', 'course_name', 'student_id', 'student_firstname', 'student_lastname',
                  'student_profile_picture')
