from django.urls import path

from .views import TeacherDetailViewSet, CourseListViewSet, TeacherTimeLineViewSet, TeacherProfileViewSet, \
    JoinRequestsViewSet

urlpatterns = [
    path('profile/', TeacherDetailViewSet.as_view()),
    path('profile-details/', TeacherProfileViewSet.as_view()),
    path('courses/', CourseListViewSet.as_view()),
    path('timeline/', TeacherTimeLineViewSet.as_view()),
    path('join-requests/', JoinRequestsViewSet.as_view()),
]
