from django.core.paginator import Paginator
from django.db.models import F
from django.db.models import Value, CharField
from django.db.models.functions import Concat
from django.http import QueryDict
from rest_framework import generics, permissions
from rest_framework.response import Response

from course.models import Comment, Like, JoinRequest
from course.serializers import CourseSerializer
from forum.models import Questions
from student_dashboard.models import Participants
from student_dashboard.models import Participants , Notifications
from users.models import CustomUser
from .models import Teacher
from .serializers import TeacherSerializers, TeacherTimeLineSerializers, TeacherProfileSerializers, \
    JoinRequestsSerializer
import time


class TeacherTimeLineViewSet(generics.ListAPIView):
    serializer_class = TeacherTimeLineSerializers
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        teacher = Teacher.objects.get(user_id=request.user.id)
        http_host = request.META['HTTP_HOST']
        q_queryset = Questions.objects.annotate(user_firstname=F('sender__user__firstname'),
                                                user_lastname=F('sender__user__lastname'),
                                                user_email=F('sender__user__email'),
                                                user_profile_picture=Concat(
                                                    Value('http://{}/media/'.format(http_host), CharField()),
                                                    F('sender__profile_picture'), output_field=CharField()),
                                                course_name=F('related_episode__chapter__course__name'),
                                                session_name=F('related_episode__post__name'),
                                                type=Value('question', CharField())
                                                ).values('user_firstname', 'user_lastname', 'user_email',
                                                         'creation_time', 'user_profile_picture', 'session_name',
                                                         'course_name', 'type').filter(
            related_episode__post__instructor_id=teacher.id).order_by('-creation_time')

        c_queryset = Comment.objects.annotate(user_firstname=F('user__firstname'),
                                              user_lastname=F('user__lastname'),
                                              user_email=F('user__email'),
                                              user_profile_picture=Concat(
                                                  Value('http://{}/media/'.format(http_host), CharField()),
                                                  F('user__Student__profile_picture'), output_field=CharField()),
                                              course_name=F('comment_post__course__name'),
                                              session_name=F('comment_post__name'),
                                              type=Value('comments', CharField())
                                              ).values('user_firstname', 'user_lastname', 'user_email', 'creation_time',
                                                       'user_profile_picture', 'session_name', 'course_name',
                                                       'type').filter(
            comment_post__course__instructor_id=teacher.id).order_by('-creation_time')

        p_queryset = Participants.objects.annotate(user_firstname=F('student__user__firstname'),
                                                   user_lastname=F('student__user__lastname'),
                                                   user_email=F('student__user__email'),
                                                   user_profile_picture=Concat(
                                                       Value('http://{}/media/'.format(http_host), CharField()),
                                                       F('student__profile_picture'), output_field=CharField()),
                                                   course_name=F('course__name'),
                                                   session_name=Value(None, CharField()),
                                                   type=Value('join', CharField())
                                                   ).values('user_firstname', 'user_lastname', 'user_email',
                                                            'creation_time', 'user_profile_picture', 'session_name',
                                                            'course_name', 'type').filter(
            course__instructor_id=teacher.id).order_by('-creation_time')

        l_queryset = Like.objects.annotate(user_firstname=F('user__user__firstname'),
                                           user_lastname=F('user__user__lastname'),
                                           user_email=F('user__user__email'),
                                           user_profile_picture=Concat(
                                               Value('http://{}/media/'.format(http_host), CharField()),
                                               F('user__profile_picture'), output_field=CharField()),
                                           course_name=F('post__course__name'),
                                           session_name=F('post__name'),
                                           type=Value('like', CharField())
                                           ).values('user_firstname', 'user_lastname', 'user_email', 'creation_time',
                                                    'user_profile_picture', 'session_name', 'course_name',
                                                    'type').filter(post__course__instructor_id=teacher.id).order_by(
            '-creation_time')

        queryset = q_queryset.union(c_queryset, p_queryset, l_queryset, all=True)
        queryset = queryset.order_by('-creation_time')
        if 'page' in list(self.request.query_params):
            paginator = Paginator(queryset, 10)
            if int(self.request.query_params['page']) <= paginator.num_pages:
                data = paginator.page(self.request.query_params['page']).object_list
                return Response(data={
                    'has_next': int(self.request.query_params['page']) < paginator.num_pages,
                    'data': data
                }
                )
        return Response(queryset)


class TeacherProfileViewSet(generics.RetrieveAPIView):
    serializer_class = TeacherProfileSerializers
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_object(self):
        if 'teacher_id' in list(self.request.query_params.keys()):
            teacher = Teacher.objects.get(pk=self.request.query_params['teacher_id'])
        else:
            user = self.request.user
            teacher = user.Teacher
        return teacher


class TeacherDetailViewSet(generics.ListAPIView, generics.UpdateAPIView):
    serializer_class = TeacherSerializers
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        if 'teacher_id' in list(self.request.query_params.keys()):
            teacher = Teacher.objects.all().get(pk=self.request.query_params['teacher_id'])
            data = self.get_serializer([teacher, ], many=True).data[0]
            return Response(data)
        else:
            custom_user = CustomUser.objects.get(pk=self.request.user.id)
            teacher_object = custom_user.Teacher
            queryset = [teacher_object, ]
            serializer = self.get_serializer(queryset, many=True)
            user_data = serializer.data[0]
            return Response(user_data)

    def get_object(self):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        teacher_object = custom_user.Teacher
        return teacher_object

    def perform_update(self, serializer):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        request_data = {}
        for key in list(self.request.data.keys()):
            request_data[key] = self.request.data[key]
        requested_data_query_dict = QueryDict('', mutable=True)
        requested_data_query_dict.update(request_data)
        if 'email' in list(requested_data_query_dict.keys()) and CustomUser.objects.filter(
                email=requested_data_query_dict['email']).first() is not None:
            requested_data_query_dict['email'] = custom_user.email
        if serializer.is_valid(raise_exception=True):
            serializer.update(custom_user, validated_data=requested_data_query_dict)
        serializer.save(data=requested_data_query_dict)


class CourseListViewSet(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Teacher.objects.all()
    serializer_class = CourseSerializer

    def perform_create(self, serializer):
        serializer.save(instructor_id=Teacher.objects.filter(user=self.request.user).first().id)

    def get_queryset(self):
        custom_user = CustomUser.objects.get(pk=self.request.user.id)
        teacher_object = custom_user.Teacher
        teacher = Teacher.objects.get(pk=teacher_object.pk)
        return teacher.Creator.all()


class JoinRequestsViewSet(generics.ListAPIView, generics.CreateAPIView):
    serializer_class = JoinRequestsSerializer

    def get_queryset(self):
        teacher = self.request.user.Teacher
        return JoinRequest.objects.filter(instructor=teacher)

    def post(self, request, *args, **kwargs):
        join_request = JoinRequest.objects.get(pk=self.request.data['id'])
        if int(self.request.data['accept']) == 1:
            student = join_request.student
            student.courses.add(join_request.course)
            student.save()
            Participants.objects.create(course=join_request.course, student=join_request.student,
                                        creation_time=int(time.time() * 1000))
            Notifications.objects.create(student=join_request.student, user=self.request.user,
                                        type=3, course=join_request.course, created_at=int(time.time() * 1000))
        else:
            Notifications.objects.create(student=join_request.student, user=self.request.user,
                                         type=4, course=join_request.course, created_at=int(time.time() * 1000))
        join_request.delete()
        data = self.get_serializer(self.request.user.Teacher.requests.all(), many=True).data
        return Response(data)
