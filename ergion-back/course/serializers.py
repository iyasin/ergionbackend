import time

from rest_framework import serializers
from student_dashboard.models import Notifications
from forum.models import Questions
from teacher_dashboard.models import Teacher
from users.models import CustomUser
from .models import Chapter, Episode
from .models import Course, News, Comment, File, Post, JoinRequest


class FileSerializer(serializers.HyperlinkedModelSerializer):
    post_id = serializers.IntegerField(source='post.pk', required=False)

    class Meta:
        model = File
        fields = ('id', 'post_id', 'file')
        read_only_fields = ('id',)

    def create(self, validated_data):
        file = File.objects.get(pk=self.data['id'])
        return file

    def to_representation(self, instance):
        data = super(FileSerializer, self).to_representation(instance)
        request = self.context.get('request', None)
        http_host = request.META['HTTP_HOST']
        if request.method == 'POST':
            file = File.objects.create(post=Post.objects.get(pk=data['post_id']), file=self.initial_data['file'])
            data['file'] = ('http://{}/media/' + str(file.file)).format(http_host)
            data['id'] = file.pk
        if request.method == 'GET' or request.method == 'POST':
            file = File.objects.all().get(pk=data['id'])
            data['size'] = file.file.size
        return data


class EpisodeSerializer(serializers.HyperlinkedModelSerializer):
    chapter_id = serializers.IntegerField(source='chapter.pk', required=False)
    files = FileSerializer(many=True, required=False, read_only=True)
    creation_time = serializers.IntegerField(source='post.creation_time', required=False)
    name = serializers.CharField(source='post.name', required=False)
    description = serializers.CharField(source='post.description', required=False)
    post_id = serializers.IntegerField(source='post.pk', required=False)

    class Meta:
        model = Episode
        fields = ('id', 'name', 'creation_time', 'description', 'chapter_id', 'files', 'episode_url', 'post_id')
        read_only_fields = ('id', 'files', 'creation_time', 'episode_url', 'post_id')

    def to_representation(self, instance):
        data = super(EpisodeSerializer, self).to_representation(instance)
        if self.get_user_id() is not None:
            user = CustomUser.objects.all().get(pk=self.get_user_id())
            episode = Episode.objects.all().get(pk=data['id'])
            data['likes_count'] = episode.post.likes.count()
            if user.role == 'S':
                data['liked'] = user.Student in episode.post.likes.all()
        episode = Episode.objects.get(pk=data['id'])
        if self.context != {}:
            files_data = []
            for file in episode.post.files.all():
                files_data.append(FileSerializer(file, context=self.context).data)
            data['files'] = files_data
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class ChapterSerializer(serializers.HyperlinkedModelSerializer):
    course_id = serializers.IntegerField(source='course.pk', required=False)
    episodes = EpisodeSerializer(source='episode', many=True, required=False, read_only=True)

    class Meta:
        model = Chapter
        fields = ('id', 'name', 'creation_time', 'course_id', 'episodes')
        read_only_fields = ('id', 'episodes')

    def to_representation(self, instance):
        data = super(ChapterSerializer, self).to_representation(instance)
        request = self.context.get('request', None)
        if request.method == 'GET':
            data['episodes'] = sorted(data['episodes'], key=lambda x: x['creation_time'])
        return data


class ReplySerializer(serializers.HyperlinkedModelSerializer):
    user_id = serializers.IntegerField(source='user.pk', required=False)
    user_profile_picture = serializers.CharField(required=False)
    user_firstname = serializers.CharField(source='user.firstname', required=False)
    user_lastname = serializers.CharField(source='user.lastname', required=False)
    parent_comment_id = serializers.IntegerField(source='replied_to.pk', required=False)
    liked = serializers.BooleanField(required=False)
    likes_count = serializers.IntegerField(required=False)

    class Meta:
        model = Comment
        fields = (
            'id', 'comment_text', 'user_id', 'user_profile_picture', 'user_firstname', 'user_lastname',
            'parent_comment_id', 'liked', 'likes_count')
        read_only_fields = fields

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id

    def to_representation(self, instance):
        data = super(ReplySerializer, self).to_representation(instance)
        user = CustomUser.objects.get(pk=data['user_id'])
        request = self.context.get('request', None)
        if request.method == 'GET' or request.method == 'POST':
            http_host = request.META['HTTP_HOST']
            if user.role == 'S':
                if str(user.Student.profile_picture.name) != '':
                    data['profile_picture'] = ('http://{}/media/' + user.Student.profile_picture.name).format(http_host)
                else:
                    data['profile_picture'] = ''
            elif user.role == 'T':
                if str(user.Teacher.profile_picture.name) != '':
                    data['profile_picture'] = ('http://{}/media/' + user.Teacher.profile_picture.name).format(http_host)
                else:
                    data['profile_picture'] = ''
            comment = Comment.objects.get(pk=data['id'])
            if self.get_user_id() is not None:
                request_user = CustomUser.objects.get(pk=self.get_user_id())
                data['liked'] = request_user in comment.likes.all()
            else:
                data['liked'] = False
            data['likes_count'] = comment.likes.count()
        return data


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    user_id = serializers.IntegerField(source='user.pk', required=False)
    replies = ReplySerializer(many=True, required=False)
    user_profile_picture = serializers.CharField(required=False)
    user_firstname = serializers.CharField(source='user.firstname', required=False)
    user_lastname = serializers.CharField(source='user.lastname', required=False)
    post_id = serializers.IntegerField(required=False)
    parent_comment_id = serializers.IntegerField(source='replied_to.pk', required=False)

    class Meta:
        model = Comment
        fields = (
            'id', 'comment_text', 'user_id', 'post_id', 'user_profile_picture', 'user_firstname',
            'user_lastname', 'replies', 'parent_comment_id', 'creation_time')
        read_only_fields = ('id', 'user_profile_picture', 'user_firstname', 'user_lastname', 'replies', 'user_id')

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id

    def to_representation(self, instance):
        data = super(CommentSerializer, self).to_representation(instance)
        user = CustomUser.objects.get(pk=data['user_id'])
        request = self.context.get('request', None)
        comment = Comment.objects.get(pk=data['id'])
        if self.get_user_id() is not None:
            request_user = CustomUser.objects.get(pk=self.get_user_id())
            data['liked'] = request_user in comment.likes.all()
        else:
            data['liked'] = False
        data['likes_count'] = comment.likes.count()
        if request.method == 'GET' or request.method == 'POST':
            http_host = request.META['HTTP_HOST']
            if user.role == 'S':
                if str(user.Student.profile_picture.name) != '':
                    data['profile_picture'] = ('http://{}/media/' + user.Student.profile_picture.name).format(http_host)
                else:
                    data['profile_picture'] = ''
            elif user.role == 'T':
                if str(user.Teacher.profile_picture.name) != '':
                    data['profile_picture'] = ('http://{}/media/' + user.Teacher.profile_picture.name).format(http_host)
                else:
                    data['profile_picture'] = ''
        return data

    def create(self, validated_data):
        comment = Comment.objects.create(user=CustomUser.objects.get(pk=self.get_user_id()),
                                         comment_text=validated_data['comment_text'],
                                         creation_time=int(time.time() * 1000))
        if 'post_id' in list(validated_data.keys()):
            post = Post.objects.get(pk=validated_data['post_id'])
            post.comments.add(comment)
            post.save()
        elif 'replied_to' in list(validated_data.keys()):
            parent_comment = Comment.objects.get(pk=validated_data['replied_to']['pk'])
            comment.replied_to = parent_comment
            comment.save()
            if parent_comment.user.role == "S":
                Notifications.objects.create(student=parent_comment.user.Student, user_id=self.get_user_id(), created_at= int(time.time() * 1000),
                                             type=1, comment=parent_comment)
        return comment


class StudentEpisodesSerializer(serializers.HyperlinkedModelSerializer):
    chapter_id = serializers.IntegerField(source='chapter.pk', required=False)
    course_id = serializers.IntegerField(source='chapter.course.pk', required=False)
    course_url = serializers.CharField(source='chapter.course.course_url', required=False)
    files = FileSerializer(many=True, required=False)
    instructor_firstname = serializers.CharField(source='chapter.course.instructor.user.firstname', required=False)
    instructor_lastname = serializers.CharField(source='chapter.course.instructor.user.lastname', required=False)
    instructor_profile_picture = serializers.ImageField('chapter.course.instructor.profile_picture', required=False)
    comments = CommentSerializer(source='post.comments', many=True, required=False)
    creation_time = serializers.IntegerField(source='post.creation_time', required=False)
    name = serializers.CharField(source='post.name', required=False)
    description = serializers.CharField(source='post.description', required=False)

    class Meta:
        model = Episode
        fields = (
            'id', 'name', 'creation_time', 'description', 'chapter_id', 'episode_url', 'course_id',
            'course_url', 'files', 'instructor_firstname', 'instructor_lastname', 'instructor_profile_picture',
            'comments', 'course_url')
        read_only_fields = fields

    def to_representation(self, instance):
        data = super(StudentEpisodesSerializer, self).to_representation(instance)
        request = self.context.get('request', None)
        http_host = request.META['HTTP_HOST']
        episode = Episode.objects.all().get(pk=data['id'])
        data['chapter_id'] = episode.chapter.pk
        chapter = Chapter.objects.all().get(pk=data['chapter_id'])
        data['course_id'] = chapter.course.pk
        data['course_url'] = chapter.course.course_url
        data['instructor_firstname'] = chapter.course.instructor.user.firstname
        data['instructor_lastname'] = chapter.course.instructor.user.lastname
        data['instructor_profile_picture'] = (
                'http://{}/media/' + chapter.course.instructor.profile_picture.name).format(http_host)
        files = []
        for file in episode.files.all():
            file_data = {
                'file': ('http://{}/media/' + file.file.name).format(http_host),
                'episode_id': data['id'],
                'id': file.pk,
                'size': file.file.size
            }
            files.append(file_data)
        data['files'] = files
        comments = episode.comments.all()
        comment_serializer = CommentSerializer(comments, many=True, context=self.context)
        user = CustomUser.objects.all().get(pk=self.get_user_id())
        episode = Episode.objects.all().get(pk=data['id'])
        if user.role == 'S':
            data['liked'] = user.Student in episode.post.likes.all()
        data['likes_count'] = episode.post.likes.count()
        data['comments'] = list(comment_serializer.data)
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class NewsSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, required=False, read_only=True)
    instructor_firstname = serializers.CharField(source='post.instructor.user.firstname', required=False)
    instructor_lastname = serializers.CharField(source='post.instructor.user.lastname', required=False)
    instructor_profile_picture = serializers.ImageField(source='post.instructor.profile_picture', required=False)
    comments = CommentSerializer(many=True, required=False, read_only=True)
    description = serializers.CharField(source='post.description', required=False)
    creation_time = serializers.IntegerField(source='post.creation_time', required=False)
    instructor_id = serializers.IntegerField(source='post.instructor.pk', required=False)
    post_id = serializers.IntegerField(source='post.pk', required=False)

    class Meta:
        model = News
        fields = ('id', 'instructor_id', 'course_id', 'files', 'creation_time', 'instructor_firstname',
                  'instructor_lastname', 'instructor_profile_picture', 'comments', 'description', 'post_id')
        read_only_fields = ('id', 'creation_time', 'files', 'instructor_firstname', 'instructor_lastname',
                            'instructor_profile_picture')

    def to_representation(self, instance):
        data = super(NewsSerializer, self).to_representation(instance)
        news = News.objects.all().get(pk=data['id'])
        post = news.post
        data['likes_count'] = post.likes.count()
        if self.get_user_id() is not None:
            request_user = CustomUser.objects.get(pk=self.get_user_id())
            if request_user.role == 'S':
                data['liked'] = request_user.Student in news.post.likes.all()
            else:
                data['liked'] = False
        else:
            data['liked'] = False
        comments = news.post.comments.all()
        comment_serializer = CommentSerializer(comments, many=True, context=self.context)
        data['comments'] = comment_serializer.data
        data['news_url'] = news.news_url
        files_data = []
        for file in post.files.all():
            files_data.append(FileSerializer(file, context=self.context).data)
        data['files'] = files_data
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class CourseSerializer(serializers.ModelSerializer):
    instructor_firstname = serializers.CharField(source='instructor.user.firstname', required=False, read_only=True)
    instructor_lastname = serializers.CharField(source='instructor.user.lastname', required=False, read_only=True)
    instructor_id = serializers.IntegerField(source='instructor.user.pk', required=False)
    instructor_profile_picture = serializers.ImageField(source='instructor.profile_picture', required=False)

    class Meta:
        model = Course
        fields = (
            'id', 'instructor_id', 'course_url', 'instructor_id', 'instructor_firstname',
            'instructor_lastname', 'grade', 'name', 'subject', 'course_cover', 'capacity', 'about_course',
            'instructor_profile_picture', 'is_public')

    def to_representation(self, instance):
        data = super(CourseSerializer, self).to_representation(instance)
        data['course_url'] = "/course/{}".format(instance.id)
        instance.course_url = "/course/{}".format(instance.id)
        course = Course.objects.all().get(pk=data['id'])
        data['students_count'] = course.course_students.count()
        episodes_count = 0
        for chapter in course.chapter.all():
            episodes_count += chapter.episode.count()
        data['episodes_count'] = episodes_count
        data['news'] = NewsSerializer(course.course_news.all(), many=True, context=self.context).data
        instance.save()
        data['count_of_course_questions'] = Questions.objects.filter(
            related_episode__post__course_id=instance.id).exclude(is_answered=True).count()
        if self.get_user_id() is not None:
            if CustomUser.objects.get(pk=self.get_user_id()).role == 'S':
                data['requested'] = JoinRequest.objects.filter(course=course, student=CustomUser.objects.get(
                    pk=self.get_user_id()).Student).first() is not None
                data['joined'] = course in CustomUser.objects.get(pk=self.get_user_id()).Student.courses.all()
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id


class CourseUpdateDeleteSerializers(serializers.ModelSerializer):
    instructor_firstname = serializers.CharField(source='instructor.user.firstname', required=False, read_only=True)
    instructor_lastname = serializers.CharField(source='instructor.user.lastname', required=False, read_only=True)
    name = serializers.CharField(required=False)
    subject = serializers.CharField(required=False)
    news = NewsSerializer(source='course_news', many=True, required=False)

    class Meta:
        model = Course
        fields = ('id', 'instructor', 'course_url', 'news', 'instructor_firstname', 'instructor_lastname',
                  'grade', 'name', 'subject', 'course_cover', 'capacity', 'about_course', 'is_public')

    def to_representation(self, instance):
        data = super(CourseUpdateDeleteSerializers, self).to_representation(instance)
        course = Course.objects.all().get(pk=data['id'])
        if self.get_user_id() is not None:
            if CustomUser.objects.get(pk=self.get_user_id()).role == 'S':
                data['requested'] = JoinRequest.objects.filter(course=course, student=CustomUser.objects.get(
                    pk=self.get_user_id()).Student).first() is not None
                data['joined'] = course in CustomUser.objects.get(pk=self.get_user_id()).Student.courses.all()
        return data

    def get_user_id(self):
        request = self.context.get('request', None)
        if request:
            return request.user.id
