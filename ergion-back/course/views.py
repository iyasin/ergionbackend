import time

from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from users.models import CustomUser
from .models import Chapter, Episode, Course, Comment, Post, News, File, Like, JoinRequest
from .models import Chapter, Episode, Course, Comment, Post, News, File, Like
from .permissions import IsTeacher
from .serializers import ChapterSerializer, EpisodeSerializer, FileSerializer, CommentSerializer, \
    CourseSerializer, CourseUpdateDeleteSerializers, NewsSerializer
from student_dashboard.models import Notifications
from users.models import CustomUser
from .models import Chapter, Episode, Course, Comment, Post, News, File, Like, JoinRequest
from .permissions import IsTeacher
from .serializers import ChapterSerializer, EpisodeSerializer, FileSerializer, CommentSerializer, \
    CourseSerializer, CourseUpdateDeleteSerializers, NewsSerializer
from django.db.models import Q


class CourseDetailsViewSet(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class AllCoursesViewSet(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseChaptersViewSet(generics.ListAPIView, generics.ListCreateAPIView, generics.UpdateAPIView,
                            generics.DestroyAPIView):
    serializer_class = ChapterSerializer

    def post(self, request, *args, **kwargs):
        chapter = Chapter.objects.create(name=request.data['name'], creation_time=int(time.time() * 1000),
                                         course=Course.objects.get(pk=request.data['course_id']))
        serializer = self.get_serializer(chapter)
        return Response(serializer.data)

    def get_object(self):
        return Chapter.objects.get(pk=self.request.data['chapter_id'])

    def get_queryset(self):
        queryset = []
        if 'course_id' in list(self.request.query_params.keys()):
            course_id = int(self.request.query_params['course_id'])
            queryset = [chapter for chapter in Chapter.objects.all().order_by('creation_time') if
                        chapter.course.pk == course_id]
        return queryset

    def delete(self, request, *args, **kwargs):
        chapter = Chapter.objects.all().filter(pk=request.query_params['chapter_id']).first()
        chapter.delete()
        return Response('Chapter deleted')


class ChapterEpisodesViewSet(generics.ListAPIView, generics.ListCreateAPIView, generics.UpdateAPIView,
                             generics.DestroyAPIView):
    serializer_class = EpisodeSerializer

    def post(self, request, *args, **kwargs):
        post = Post.objects.create(name=request.data['name'], creation_time=int(time.time() * 1000),
                                   description=request.data['description'],
                                   course=Chapter.objects.get(pk=request.data['chapter_id']).course, post_type=0,
                                   instructor=self.request.user.Teacher)
        episode = Episode.objects.create(post=post, chapter=Chapter.objects.get(pk=request.data['chapter_id']), )
        episode.episode_url = '/chapter/' + str(request.data['chapter_id']) + '/episode/' + str(episode.pk) + '/'
        episode.save()
        serializer = self.get_serializer(episode)
        return Response(serializer.data)

    def get_object(self):
        return Episode.objects.get(pk=self.request.data['episode_id'])

    def get_queryset(self):
        queryset = []
        if 'chapter_id' in list(self.request.query_params.keys()):
            chapter_id = int(self.request.query_params['chapter_id'])
            queryset = [episode for episode in Episode.objects.all().order_by('pk') if
                        episode.chapter.pk == chapter_id]

        return queryset

    def update(self, request, *args, **kwargs):
        episode = Episode.objects.all().get(pk=request.data.get('episode_id'))
        post = episode.post
        post.name = request.data.get('name', post.name)
        post.description = request.data.get('description', post.description)
        post.save()
        episode.save()
        serializer = self.get_serializer(episode)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        episode = Episode.objects.all().filter(pk=request.query_params['episode_id']).first()
        post = episode.post
        episode.delete()
        post.delete()
        return Response('Episode deleted')


class FileViewSet(generics.ListAPIView, generics.ListCreateAPIView, generics.UpdateAPIView,
                  generics.DestroyAPIView):
    serializer_class = FileSerializer

    def delete(self, request, *args, **kwargs):
        file = File.objects.all().filter(pk=request.query_params['file_id']).first()
        file.delete()
        return Response('File deleted')

    def get_queryset(self):
        queryset = []
        if 'post_id' in list(self.request.query_params.keys()):
            post_id = int(self.request.query_params['post_id'])
            queryset = [post_file for post_file in File.objects.all() if post_file.post.pk == post_id]
        return queryset


class CommentViewSet(generics.ListAPIView, generics.CreateAPIView, generics.DestroyAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        post_id = self.request.query_params['post_id']
        post = Post.objects.get(pk=post_id)
        queryset = post.comments.all()
        return queryset

    def delete(self, request, *args, **kwargs):
        comment = Comment.objects.get(pk=request.query_params['comment_id'])
        comment.delete()
        return Response('Comment deleted')


class CommentLikeViewSet(generics.UpdateAPIView):
    serializer_class = CommentSerializer

    def update(self, request, *args, **kwargs):
        comment = Comment.objects.get(pk=request.data['comment_id'])
        user = CustomUser.objects.get(pk=request.user.id)
        if user in comment.likes.all():
            comment.likes.remove(user)
            if comment.user.role == 'T':
                teacher = comment.user.Teacher
                teacher.popularity_index = teacher.popularity_index - 1
                teacher.save()
            if comment.user.role == 'S':
                Notifications.objects.filter(Q(student=comment.user.Student) & Q(user=request.user) & Q(comment=comment)
                                             & Q(type=0)).delete()
        else:
            comment.likes.add(user)
            if comment.user.role == 'T':
                teacher = comment.user.Teacher
                teacher.popularity_index = teacher.popularity_index + 1
                teacher.save()
            if comment.user.role == 'S':
                Notifications.objects.create(student=comment.user.Student, user=request.user,
                                             type=0, created_at=int(time.time() * 1000), comment=comment)
        comment.save()
        serializer = self.get_serializer([comment, ], many=True)
        return Response(serializer.data[0])


class CourseUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsTeacher)
    queryset = Course.objects.all().order_by('id')
    serializer_class = CourseUpdateDeleteSerializers

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response('Course deleted', status=status.HTTP_204_NO_CONTENT)

    def perform_update(self, serializer):
        course = serializer.save()
        if 'is_public' in list(self.request.data.keys()) and int(self.request.data['is_public']) == 1:
            for request in JoinRequest.objects.all().filter(course=course):
                student = request.student
                student.courses.add(course)
                request.delete()

class NewsViewSet(generics.ListCreateAPIView, generics.DestroyAPIView, generics.UpdateAPIView):
    queryset = News.objects.all().order_by('id')
    serializer_class = NewsSerializer

    def create(self, request, *args, **kwargs):
        post = Post.objects.create(description=request.data['description'], creation_time=int(time.time() * 1000),
                                   course=Course.objects.get(pk=request.data['course_id']), post_type=1,
                                   instructor=self.request.user.Teacher)
        news = News.objects.create(post=post, course=Course.objects.get(pk=request.data['course_id']))
        news.news_url = '/news/' + str(news.pk) + '/'
        news.save()
        serializer = self.get_serializer(news)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        news = News.objects.get(pk=request.query_params['news_id'])
        post = news.post
        news.delete()
        post.delete()
        return Response('News deleted')

    def update(self, request, *args, **kwargs):
        news = News.objects.all().get(pk=request.data.get('news_id'))
        post = news.post
        post.name = request.data.get('name', post.name)
        post.save()
        news.save()
        serializer = self.get_serializer(news)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = []
        if 'course_id' in list(self.request.query_params.keys()):
            queryset = News.objects.filter(course_id=self.request.query_params['course_id'])
        return queryset


class BaseManageView(APIView):

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'VIEWS_BY_METHOD'):
            raise Exception('VIEWS_BY_METHOD static dictionary variable must be defined on a ManageView class!')
        if request.method in self.VIEWS_BY_METHOD:
            return self.VIEWS_BY_METHOD[request.method]()(request, *args, **kwargs)

        return Response(status=405)


class CourseManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': CourseUpdateDelete.as_view,
        'GET': CourseDetailsViewSet.as_view,
        'PUT': CourseUpdateDelete.as_view,
        'PATCH': CourseUpdateDelete.as_view
    }


class NewsDetailsViewSet(generics.RetrieveAPIView):
    serializer_class = NewsSerializer

    def get_object(self):
        return News.objects.all().get(pk=self.request.query_params['news_id'])


class PostLikeViewSet(generics.UpdateAPIView):
    serializer_class = NewsSerializer

    def update(self, request, *args, **kwargs):
        post = Post.objects.get(pk=request.data['post_id'])
        user = CustomUser.objects.get(pk=request.user.id)
        student = user.Student
        if student in post.likes.all():
            post.likes.remove(student)
            like = Like.objects.filter(user_id=student.id, post=post)
            for like_object in like:
                like_object.delete()
            teacher = post.instructor
            teacher.popularity_index = teacher.popularity_index - 1
            teacher.save()
        else:
            post.likes.add(student)
            Like.objects.create(user_id=student.id,
                                post_id=post.id,
                                creation_time=int(time.time() * 1000))
            teacher = post.instructor
            teacher.popularity_index = teacher.popularity_index + 1
            teacher.save()
        post.save()
        if post.post_type == 1:
            news = post.post_news.all()[0]
            return Response(self.get_serializer(news).data)
        else:
            episode = post.post_episode.all()[0]
            data = EpisodeSerializer(episode).data
            data['liked'] = student in post.likes.all()
            data['liked_count'] = post.likes.count()
            return Response(data)
