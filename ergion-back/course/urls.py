from django.urls import path

from .views import CourseChaptersViewSet, ChapterEpisodesViewSet, CommentLikeViewSet, AllCoursesViewSet, \
    CourseManageView, NewsViewSet, CommentViewSet, FileViewSet, NewsDetailsViewSet, PostLikeViewSet


urlpatterns = [
    path('chapters/', CourseChaptersViewSet.as_view()),
    path('chapter-episodes/', ChapterEpisodesViewSet.as_view()),
    path('post-files/', FileViewSet.as_view()),
    path('comments/', CommentViewSet.as_view()),
    path('comment-likes/', CommentLikeViewSet.as_view()),
    path('', AllCoursesViewSet.as_view()),
    path('<int:pk>', CourseManageView.as_view()),
    path('news/', NewsViewSet.as_view()),
    path('news-details/', NewsDetailsViewSet.as_view()),
    path('post-likes/', PostLikeViewSet.as_view()),
]
