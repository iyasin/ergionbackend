from django.db import models
from django.utils.translation import ugettext_lazy as _

from student_dashboard.models import Student
from teacher_dashboard.models import Teacher
from users.models import CustomUser


class Course(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    subject = models.CharField(max_length=100, null=False, blank=False)
    course_cover = models.ImageField(upload_to="Uploaded/CoursePoster", null=True, blank=True, default=None)
    capacity = models.IntegerField(null=False, blank=False, default=10)
    instructor = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="Creator", default=1)
    about_course = models.TextField(_("about course"), null=True)
    course_url = models.CharField(_("course link"), max_length=100, null=True, blank=True, default=None)
    grade = models.IntegerField(null=False, choices=[(x, "class {}".format(x)) for x in range(1, 13)], default=1)
    is_public = models.BooleanField(null=False, blank=False, default=True)

    def __str__(self):
        return self.name


class JoinRequest(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="requests", blank=False, null=False)
    instructor = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="requests", blank=False, null=False)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="requests", blank=False, null=False)

    def __str__(self):
        return self.course.name + ' : ' + self.student.user.firstname + " " + self.student.user.lastname


class Chapter(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    creation_time = models.BigIntegerField(null=False, blank=False, default=0)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='chapter', default=None)

    def __str__(self):
        return self.name


class Comment(models.Model):
    comment_text = models.TextField()
    replied_to = models.ForeignKey('self', default=None, blank=True, null=True, related_name='replies',
                                   on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, related_name='comments', on_delete=models.CASCADE, default=1)
    likes = models.ManyToManyField(CustomUser, related_name='liked_comments')
    creation_time = models.BigIntegerField(null=False, blank=False, default=0)

    def __str__(self):
        return "{}: {}".format(self.user.pk, self.comment_text)


class Post(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    creation_time = models.BigIntegerField(null=True, blank=True, default=0)
    comments = models.ManyToManyField(Comment, related_name='comment_post')
    likes = models.ManyToManyField(Student, related_name='liked_posts', default=None)
    instructor = models.ForeignKey(Teacher, related_name='posts', on_delete=models.CASCADE, default=1)
    description = models.TextField(blank=True, null=True)
    course = models.ForeignKey(Course, related_name='course_posts', on_delete=models.CASCADE)
    post_type = models.IntegerField(default=0)

    def __str__(self):
        return 'Post: ' + str(self.pk)


class Episode(models.Model):
    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE, related_name='episode', default=None)
    episode_url = models.CharField(_("episode link"), max_length=100, null=True, blank=True, default=None)
    post = models.ForeignKey(Post, related_name='post_episode', on_delete=models.CASCADE)

    def __str__(self):
        return self.post.name


class News(models.Model):
    course = models.ForeignKey(Course, related_name='course_news', on_delete=models.CASCADE)
    news_url = models.CharField(_("news link"), max_length=100, null=True, blank=True, default=None)
    post = models.ForeignKey(Post, related_name='post_news', on_delete=models.CASCADE)

    def __str__(self):
        return 'News: ' + str(self.pk)


class File(models.Model):
    file = models.FileField(upload_to='Uploaded/PostFiles')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='files', default=None)

    def __str__(self):
        return str(self.file)


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='Post', default=None)
    user = models.ForeignKey(Student, related_name='student_likes', default=None, on_delete=models.CASCADE)
    creation_time = models.BigIntegerField(null=True, blank=True, default=0)
