from django.contrib import admin

from .models import Course, Chapter, Episode, Comment, News, File, Post, JoinRequest

admin.site.register(Course)
admin.site.register(Chapter)
admin.site.register(Episode)
admin.site.register(News)
admin.site.register(Comment)
admin.site.register(File)
admin.site.register(Post)
admin.site.register(JoinRequest)
